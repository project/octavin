<?php
?>

<div id="block-<?php print $block->module .'-'. $block->delta; ?>" class="block block-<?php print $block->module; ?> <?php print $block_zebra; ?> <?php print $position; ?> <?php print $skinr; ?>">
  <div class="inner">
  <div class="block-top-left">
  <div class="block-top-right">
  <div class="block-bottom-left">
  <div class="block-bottom-right">
  <div class="block-boder-left">
  <div class="block-boder-right">
    <?php if ($block->subject): ?>
    <div class="block-title-left"><div class="block-title-right"><h2 class="title block-title"><?php print $block->subject; ?></h2></div></div>
    <?php endif;?>
    <div class="content">
      <?php print $block->content; ?>
    </div>
  </div></div></div></div></div></div>
  </div><!-- /block-inner -->
</div><!-- /block -->
